﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HrWebb.Models;
using Microsoft.EntityFrameworkCore;

namespace HrWebb.EntityFramwork
{
    public class DataContext :DbContext
    {
        public DataContext() { }
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
       virtual public DbSet<JobApplication> JobApplications { get; set; }
       virtual public DbSet<JobOffer> JobOfers { get; set; }
       virtual public DbSet<Company> Companies { get; set; }
    }
}
