﻿using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HrManagement.Helpers
{
    public class FileUploader
    {
        private readonly string connectionString;

        public FileUploader(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public async Task UploadFile(IFormFile formFile)
        {
            byte[] file = new byte[formFile.Length];
            using (var stream = formFile.OpenReadStream())
            {
                stream.Read(file, 0, (int)formFile.Length);
            }
            
            if (CloudStorageAccount.TryParse(connectionString, out CloudStorageAccount storageAccount))
            {
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = cloudBlobClient.GetContainerReference("applications");
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(Path.GetFileName(formFile.FileName));
                await blockBlob.UploadFromByteArrayAsync(file, 0, file.Length);
            }
        }
    }
}
