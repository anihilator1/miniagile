﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HrManagement.Helpers;
using HrWebb.EntityFramwork;
using HrWebb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HrWebb.Controllers
{
    [Route("[controller]/[action]")]
    public class JobApplicationController : Controller
    {
        private readonly DataContext _context;
        private readonly FileUploader fileUploader;
        public JobApplicationController(IConfiguration configuration, DataContext context)
        {
            _context = context;
            fileUploader = new FileUploader(configuration.GetSection("StorageConnectionString").Get<string>());

        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<ActionResult> Create(int k)
        {
            JobApplication jobApplication = new JobApplication();
            jobApplication.OfferId = k;
            return View(jobApplication);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(JobApplication model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            _context.JobApplications.Add(model);
            await _context.SaveChangesAsync();
            await fileUploader.UploadFile(model.File);
            return RedirectToAction("Index", "JobOffer");
        }
        [HttpGet]
        public IActionResult Details(int id)
        {
            JobApplication jobApplication =_context.JobApplications.ToList().Find(o => o.Id == id);
            return View(jobApplication);
        }
    }
}