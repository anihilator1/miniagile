﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HrWebb.EntityFramwork;
using HrWebb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HrWebb.Controllers
{
    [Route("[controller]/[action]")]
    public class JobOfferController : Controller
    {
        private readonly DataContext _context;
        public JobOfferController(DataContext context)
        {
            _context = context;
        }
        [HttpGet]
        public IActionResult Details(int id)
        {
            var offer = _context.JobOfers.Include(jobOffer => jobOffer.Company).FirstOrDefault(o => o.Id == id);
            if (offer == null) return NotFound();
            return View(offer);
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return BadRequest();
            var offer = _context.JobOfers.ToList().Find(j => j.Id == id);
            if (offer == null) return NotFound();
            return View(offer);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(JobOffer model)
        {
            var offer = _context.JobOfers.ToList().Find(j => j.Id == model.Id);
            offer.JobTitle = model.JobTitle;
            offer.Description = model.Description;
            _context.Update(offer);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", new { id = model.Id });
        }
        [HttpPost]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return BadRequest();
            var offer = _context.JobOfers.ToList().Find(j => j.Id == id);
            _context.JobOfers.Remove(offer);
            _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            var model = new JobOfferCreateView
            {
                Companies = _context.Companies.ToList()
            };
            model.Created = DateTime.Now;
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(JobOfferCreateView model)
        {
            if (!ModelState.IsValid)
            {
                model.Companies = _context.Companies.ToList();
                return View(model);
            }

            JobOffer jobOffer = new JobOffer
            {
                CompanyId = model.CompanyId,
                Description = model.Description,
                JobTitle = model.JobTitle,
                Location = model.Location,
                SalaryFrom = model.SalaryFrom,
                SalaryTo = model.SalaryTo,
                ValidUntil = model.ValidUntil,
                Created = DateTime.Now
            };
            _context.JobOfers.Add(jobOffer);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public OffersPage GetOffers([FromQuery(Name = "search")] string searchString,int pageNumber)
        {
            IQueryable<JobOffer> preResult;
            if(String.IsNullOrEmpty(searchString))
            {
                preResult = _context.JobOfers;
            }
            else
            {
                preResult = _context.JobOfers.Where(jobOffer => jobOffer.JobTitle.Contains(searchString));
            }


            int pageSize = 4;
            int numberOfOffers = preResult.Count();
            int numberOfPages = (int)Math.Ceiling(((double)numberOfOffers) / ((double)pageSize));

            List<JobOffer> resultList = preResult.OrderBy(jobOffer => jobOffer.JobTitle).Skip((pageSize * (pageNumber-1))).Take(pageSize).Include(jobOffer => jobOffer.Company).ToList();
            OffersPage offersPage =new OffersPage(numberOfPages, pageNumber, resultList);
            return offersPage;
        }
        [HttpGet]
        public IEnumerable<JobApplication> GetApplications(int id)
        {
            return _context.JobApplications.Where(jobApplication=>jobApplication.OfferId==id).ToList();
        }
    }
}
