﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HrWebb.EntityFramwork;
using HrWebb.Models;
using Microsoft.AspNetCore.Mvc;

namespace HrWebb.Controllers
{
    public class CompaniesController : Controller
    {
        private readonly DataContext _context;
        public CompaniesController(DataContext context)
        {
            _context = context;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View(_context.Companies.ToList());
        }
    
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            Company company = _context.Companies.ToList().Find(i => i.Id == id);
            var jobOfferList = _context.JobOfers.ToList();
            foreach(var offer in jobOfferList)
            {
               if(offer.CompanyId==company.Id)
                {
                    _context.JobOfers.Remove(offer);
                }
            }
            _context.Companies.Remove(company);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            Company company = new Company();
            return View(company);
        }
        [HttpPost]
        public async Task<ActionResult> Create(Company model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            _context.Companies.Add(model);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return BadRequest();
            var offer = _context.Companies.ToList().Find(j => j.Id == id);
            if (offer == null) return NotFound();
            return View(offer);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Company model)
        {
           
            _context.Update(model);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}