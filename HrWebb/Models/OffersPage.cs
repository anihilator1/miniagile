﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrWebb.Models
{
    public class OffersPage
    {
        public int NumberOfPages { get; set; }
        public int PageNumber { get; set; }
        public IEnumerable<JobOffer> JobOffers { get; set; }
        public OffersPage(int numberOfPages,int pageNumber,IEnumerable<JobOffer> joboffers)
        {
            this.NumberOfPages = numberOfPages;
            this.PageNumber = pageNumber;
            this.JobOffers = joboffers;
        }
    }
}
