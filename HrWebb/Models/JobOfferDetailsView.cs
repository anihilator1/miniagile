﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrWebb.Models
{
    public class JobOfferDetailsView :JobOffer
    {
        public List<JobApplication> jobApplications;
    }
}
