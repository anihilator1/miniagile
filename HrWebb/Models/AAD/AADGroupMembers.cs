﻿using System.Collections.Generic;

namespace HrWebb.Models.AAD
{
    public class AADGroupMembers
    {
        public List<Value> value { get; set; }
    }
    public class Value
    {
        public string url { get; set; }
    }
}
