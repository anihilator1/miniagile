using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;

namespace Tests
{
    public class Tests
    {
        private IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new FirefoxDriver(@"C:\Users\�ukasz\Downloads");
        }

        [Test, Order(1)]
        public void AddCompany()
        {
            driver.Url = "https://offerjob.azurewebsites.net/Companies/Create";
            IWebElement nameInput = driver.FindElement(By.Id("Name"));
            IWebElement submitButton = driver.FindElement(By.XPath("//button[. = 'Create']"));
            nameInput.SendKeys("SeleniumTestName");
            submitButton.Click();
            IWebElement nameOfAddedCompany;
            try
            {
                nameOfAddedCompany = driver.FindElement(By.XPath("//td[normalize-space() = 'SeleniumTestName']"));
            }
            catch(Exception e)
            {
                nameOfAddedCompany = null;
            }

            Assert.IsNotNull(nameOfAddedCompany);
        }
        [Test, Order(2)]
        public void EditCompany()
        {
            driver.Url = "https://offerjob.azurewebsites.net/Companies";

            IEnumerable<IWebElement> deletedCompanies = driver.FindElements(By.XPath("//td[normalize-space() = 'SeleniumTestNamecos']"));
            int count = 0;
            foreach (var company in deletedCompanies)
            {
                count++;
            }

            IWebElement nameOfAddedCompany = driver.FindElement(By.XPath("//td[normalize-space() = 'SeleniumTestName']"));
            IWebElement rowOfAddedCompany = nameOfAddedCompany.FindElement(By.XPath(".."));
            IWebElement editCompanyButton = rowOfAddedCompany.FindElement(By.XPath(".//a[normalize-space() = 'Edit']"));
            editCompanyButton.Click();
            IWebElement name = driver.FindElement(By.Id("Name"));
            name.SendKeys("cos");
            IWebElement saveButton = driver.FindElement(By.XPath(".//input[@value = 'Save']"));
            saveButton.Click();

            int count1 = 0;
            deletedCompanies = driver.FindElements(By.XPath("//td[normalize-space() = 'SeleniumTestNamecos']"));
            foreach (var company in deletedCompanies)
            {
                count1++;
            }
            Assert.AreEqual(count +1, count1);
        }
        [Test, Order(3)]
        public void DeleteCompany()
        {
            driver.Url = "https://offerjob.azurewebsites.net/Companies";

            IWebElement nameOfAddedCompany = driver.FindElement(By.XPath("//td[normalize-space() = 'SeleniumTestNamecos']"));
            IWebElement rowOfAddedCompany = nameOfAddedCompany.FindElement(By.XPath(".."));
            IWebElement deleteCompanyButton = rowOfAddedCompany.FindElement(By.XPath(".//a[normalize-space() = 'Delete']"));
            IEnumerable<IWebElement> deletedCompanies = driver.FindElements(By.XPath("//td[normalize-space() = 'SeleniumTestNamecos']"));
            int count = 0;
            foreach(var company in deletedCompanies)
            {
                count++;
            }
            deleteCompanyButton.Click();
            int count1 = 0;
            deletedCompanies = driver.FindElements(By.XPath("//td[normalize-space() = 'SeleniumTestNamecos']"));
            foreach (var company in deletedCompanies)
            {
                count1++;
            }
            Assert.AreEqual(count - 1, count1);
        }
       
    }
}