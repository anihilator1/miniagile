 using HrWebb.Controllers;
using HrWebb.EntityFramwork;
using HrWebb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UnitTests
{
    [TestClass]
    public class JobOfferControlerTests
    {
        [TestMethod]
        public void BadRequestWhenIdNull ()
        {
            var mockRepo = MockJobOffers();
            var jobOfferController = new JobOfferController(mockRepo.Object);

            var result = jobOfferController.Edit((int?)null);
            Assert.IsTrue(result is BadRequestResult);
        }

        [TestMethod]
        public void EditExistingOffer()
        {
            var mockRepo = MockJobOffers();
            var jobOfferController = new JobOfferController(mockRepo.Object);

            var result = jobOfferController.Edit(1);
            Assert.IsTrue(result is ViewResult);
        }
        [TestMethod]
        public async Task InvalidModel()
        {
            var mockRepo = MockJobOffers();
            var jobOfferController = new JobOfferController(mockRepo.Object);
            jobOfferController.ModelState.AddModelError("Description", "Minimu 100 charakters");

            var modelView = new JobOfferCreateView()
            {
                Id = 1,
                JobTitle = "dev",
                CompanyId = 1,
                Company = new Company
                {
                    Id = 1,
                    Name = "Microsoft"
                },
                SalaryFrom = 100,
                SalaryTo = 200,
                Created = DateTime.Now,
                Location = "Berlin",
                Description = "adsadsadsadasfa",
                ValidUntil = DateTime.Now.AddDays(30),
                Companies = new List<Company>()
            };

    

            var result =  await jobOfferController.Create(modelView);
            Assert.IsTrue(result is ViewResult);
        }
        [TestMethod]
        public async Task JobOfferController_Edit_ReturnsRedirectToActionDetails_WhenModelStateIsValid()
        {
            var mockRepo = MockJobOffers();
            var jobOfferController = new JobOfferController(mockRepo.Object);
            var mockOffer = TestOffer();

            var result = await jobOfferController.Edit(mockOffer);
            Assert.IsTrue(result is RedirectToActionResult);

            var redirect = result as RedirectToActionResult;
            Assert.AreEqual("Details", redirect.ActionName);
        }

        public Mock<DbSet<T>> PrepareMockDbSet<T>(IQueryable<T> data) where T : class
        {
            var mockSet = new Mock<DbSet<T>>();
            var mockQueryable = mockSet.As<IQueryable<T>>();

            mockQueryable.Setup(q => q.Provider).Returns(data.Provider);
            mockQueryable.Setup(q => q.Expression).Returns(data.Expression);
            mockQueryable.Setup(q => q.ElementType).Returns(data.ElementType);
            mockQueryable.Setup(q => q.GetEnumerator()).Returns(data.GetEnumerator());

            return mockSet;
        }
        private Mock<DataContext> MockJobOffers()
        {
            var mockRepo = new Mock<DataContext>();
            var mockSet = PrepareMockDbSet(TestOffers().AsQueryable());
            var mockSet1 = PrepareMockDbSet(TestCompanies().AsQueryable());

            mockRepo.Setup(repo => repo.JobOfers).Returns(mockSet.Object);
            mockRepo.Setup(repo => repo.Companies).Returns(mockSet1.Object);


            return mockRepo;
        }

        private IEnumerable<Company> TestCompanies()
        {
            return new List<Company>
            {
               new Company{
                   Id=1,
                   Name="Microsoft" }
            };
        }
        private IEnumerable<JobOffer> TestOffers()
        {
            return new List<JobOffer>
            {
                TestOffer()
            };
        }

        private JobOffer TestOffer()
        {
            return new JobOffer
            {
                Id = 1,
                JobTitle = "dev",
                CompanyId = 1,
                Company = new Company{
                    Id=1,
                    Name="Microsoft" },
                SalaryFrom = 100,
                SalaryTo = 200,
                Created = DateTime.Now,
                Location = "Berlin",
                Description = "adsadsadsadasfa",
                ValidUntil = DateTime.Now.AddDays(30)
            };
        }
    }
}
